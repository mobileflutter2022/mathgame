import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'level.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<Offset> hoverAnimation;

  //list fo products
  List<Product> products = [
    Product(
      path: './images/pilot2.png',
    ),
  ];
  List<Product> products2 = [
    Product(
      path: './images/pilot3.png',
    ),
  ];
  int currentIndex = 0;

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 700),
    )..repeat(
        reverse: true,
      );
    hoverAnimation =
        Tween(begin: const Offset(0, 0), end: const Offset(0, 0.02))
            .animate(animationController);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    const colorizeColors = [
      Colors.white,
      Colors.purple,
    ];
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          ? Container(
              child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('./img/background.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const SizedBox(
                          height: 10, //ข้อความ
                        ),
                        DefaultTextStyle(
                          style: const TextStyle(
                              fontSize: 35.0,
                              color: Color.fromARGB(255, 255, 255, 255)),
                          child: AnimatedTextKit(
                            animatedTexts: [
                              WavyAnimatedText('MATH MASTER'),
                            ],
                            isRepeatingAnimation: true,
                          ),
                        ),
                        const SizedBox(
                          height: 90, //รูป
                        ),
                        SlideTransition(
                          position: hoverAnimation,
                          child: AnimatedSwitcher(
                            duration: const Duration(milliseconds: 7),
                            child: Image.asset(
                              products[currentIndex].path,
                              key: ValueKey<int>(currentIndex),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 60,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            textStyle: const TextStyle(fontSize: 25),
                            fixedSize: Size(170, 60),
                            backgroundColor: Color.fromARGB(255, 199, 75, 224),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => new Level()));
                          },
                          child: AnimatedTextKit(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                        builder: (context) => new Level()));
                              },
                              repeatForever: true,
                              animatedTexts: [
                                ColorizeAnimatedText(
                                  'เริ่มเกม',
                                  textStyle: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  colors: colorizeColors,
                                ),
                              ],
                              isRepeatingAnimation: true),
                        ),
                      ],
                    ))
                  ],
                ),
              ),
            )
          : SafeArea(
              //นอน
              child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('./img/background.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        DefaultTextStyle(
                          style: TextStyle(
                              fontSize: screenWidth > 1024 ? 60 : 17.0,
                              color: Color.fromARGB(255, 255, 255, 255)),
                          child: AnimatedTextKit(
                            animatedTexts: [
                              WavyAnimatedText('MATH MASTER'),
                            ],
                            isRepeatingAnimation: true,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SlideTransition(
                          position: hoverAnimation,
                          child: AnimatedSwitcher(
                            duration: const Duration(milliseconds: 7),
                            child: Image.asset(
                              products2[currentIndex].path,
                              key: ValueKey<int>(currentIndex),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            textStyle: const TextStyle(fontSize: 25),
                            fixedSize: Size(150, 40),
                            backgroundColor: Color.fromARGB(255, 199, 75, 224),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => new Level()));
                          },
                          child: AnimatedTextKit(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                        builder: (context) => new Level()));
                              },
                              repeatForever: true,
                              animatedTexts: [
                                ColorizeAnimatedText(
                                  'เริ่มเกม',
                                  textStyle: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  colors: colorizeColors,
                                ),
                              ],
                              isRepeatingAnimation: true),
                        ),
                      ],
                    ))
                  ],
                ),
              ),
            );
    });
  }
}

class Product {
  final String path;

  Product({required this.path});
}
