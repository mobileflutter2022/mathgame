import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GameData with ChangeNotifier {
  int level = 0;
  int score = 0;
  int ans = -999;
  var isShow = false;
  String pic = 'assets/images/correct.png';
  String operator = "";
  var choice = List.filled(4, -888, growable: false);
  int? varA;
  int? varB;

  void randomEquation() {
    var randomNum = new Random();
    if (level == 1) {
      var opt = new Random();
      varA = randomNum.nextInt(11);
      varB = randomNum.nextInt(11);
      if (opt.nextInt(2) == 0) {
        operator = "+";
      } else {
        operator = "-";
      }

      if (operator == '+') {
        ans = varA! + varB!;
      } else if (operator == '-') {
        ans = varA! - varB!;
      }
      choice[0] = ans;
      choice[1] = (-20 + randomNum.nextInt(20 - (-20)));
      choice[2] = (-20 + randomNum.nextInt(20 - (-20)));
      choice[3] = (-20 + randomNum.nextInt(20 - (-20)));
      choice.shuffle();
    } else if (level == 2) {
      var opt = new Random();
      varA = randomNum.nextInt(100);
      varB = randomNum.nextInt(100);
      if (opt.nextInt(2) == 0) {
        operator = "+";
      } else {
        operator = "-";
      }

      if (operator == '+') {
        ans = varA! + varB!;
      } else if (operator == '-') {
        ans = varA! - varB!;
      }
      choice[0] = ans;
      choice[1] = (-200 + randomNum.nextInt(200 - (-200)));
      choice[2] = (-200 + randomNum.nextInt(200 - (-200)));
      choice[3] = (-200 + randomNum.nextInt(200 - (-200)));
      choice.shuffle();
    } else if (level == 3) {
      var opt = new Random();
      varA = randomNum.nextInt(30);
      varB = randomNum.nextInt(20);
      if (opt.nextInt(3) == 0) {
        operator = "+";
      } else if (opt.nextInt(3) == 1) {
        operator = "-";
      } else {
        operator = "x";
      }

      if (operator == '+') {
        ans = varA! + varB!;
      } else if (operator == '-') {
        ans = varA! - varB!;
      } else if (operator == 'x') {
        ans = varA! * varB!;
      }
      choice[0] = ans;
      choice[1] = (-600 + randomNum.nextInt(600 - (-600)));
      choice[2] = (-600 + randomNum.nextInt(600 - (-600)));
      choice[3] = (-600 + randomNum.nextInt(600 - (-600)));
      choice.shuffle();
    }
  }

  void checkAns(int number) {
    if (number == ans) {
      pic = 'assets/images/correct.png';
    } else {
      pic = 'assets/images/wrong.png';
    }

    isShow = !isShow;
    Timer(
      Duration(milliseconds: 500),
      () {
        isShow = !isShow;
      },
    );
  }

  void increScore() {
    score++;
    notifyListeners();
  }
}
