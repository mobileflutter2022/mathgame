import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:math_game/home.dart';
import 'score.dart';
import 'package:math_game/game.dart';
import 'package:math_game/level.dart';
import 'package:provider/provider.dart';

import './game_provider.dart';

void main() {
  runApp(ChangeNotifierProvider<GameData>(
    create: (BuildContext context) => GameData(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
          useInheritedMediaQuery: true,
          debugShowCheckedModeBanner: false,
          builder: DevicePreview.appBuilder,
          locale: DevicePreview.locale(context),
          title: 'Math Master',
          theme: ThemeData(primarySwatch: Colors.purple),
          home: const HomePage()),
    );
  }
}
