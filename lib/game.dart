import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:math_game/score.dart';
import 'score.dart';
import 'package:provider/provider.dart';

import './game_provider.dart';

var randomNum = new Random();

class Game extends StatefulWidget {
  Game({Key? key}) : super(key: key) {
    // level = i;
  }

  @override
  State<Game> createState() => _GameState();
}

class _GameState extends State<Game> {
  late Timer _timer;
  int _start = 60;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final gameData = Provider.of<GameData>(context, listen: false);
      startTimer();
      setState(() {
        gameData.randomEquation();
      });
    });
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Score()),
          );
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final gameData = Provider.of<GameData>(context);

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    var portraitSize = 0.3;
    var landscapeSize = 0.15;
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    Widget choiceBtn1() {
      return ShaderMask(
        shaderCallback: (bounds) =>
            LinearGradient(colors: [Colors.blue, Colors.teal]).createShader(
          bounds,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: SizedBox(
            width: isPortrait
                ? screenWidth * portraitSize
                : screenWidth * landscapeSize,
            height: screenHeight * 0.1,
            child: ElevatedButton(
              onPressed: gameData.isShow == false
                  ? () => setState(() {
                        if (gameData.choice[0] == gameData.ans) {
                          gameData.increScore();
                        }
                        gameData.checkAns(gameData.choice[0]);
                        gameData.randomEquation();
                      })
                  : null,
              child: Text(
                '${gameData.choice[0]}',
                style: TextStyle(fontSize: 30),
              ),
            ),
          ),
        ),
      );
    }

    Widget choiceBtn2() {
      return ShaderMask(
        shaderCallback: (bounds) =>
            LinearGradient(colors: [Colors.blue, Colors.teal]).createShader(
          bounds,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: SizedBox(
            width: isPortrait
                ? screenWidth * portraitSize
                : screenWidth * landscapeSize,
            height: screenHeight * 0.1,
            child: ElevatedButton(
              onPressed: gameData.isShow == false
                  ? () => setState(() {
                        if (gameData.choice[1] == gameData.ans) {
                          gameData.increScore();
                        }
                        gameData.checkAns(gameData.choice[1]);
                        gameData.randomEquation();
                      })
                  : null,
              child: Text(
                '${gameData.choice[1]}',
                style: TextStyle(fontSize: 30),
              ),
            ),
          ),
        ),
      );
    }

    Widget choiceBtn3() {
      return ShaderMask(
        shaderCallback: (bounds) =>
            LinearGradient(colors: [Colors.blue, Colors.teal]).createShader(
          bounds,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: SizedBox(
            width: isPortrait
                ? screenWidth * portraitSize
                : screenWidth * landscapeSize,
            height: screenHeight * 0.1,
            child: ElevatedButton(
              onPressed: gameData.isShow == false
                  ? () => setState(() {
                        if (gameData.choice[2] == gameData.ans) {
                          gameData.increScore();
                        }
                        gameData.checkAns(gameData.choice[2]);
                        gameData.randomEquation();
                      })
                  : null,
              child: Text(
                '${gameData.choice[2]}',
                style: TextStyle(fontSize: 30),
              ),
            ),
          ),
        ),
      );
    }

    Widget choiceBtn4() {
      return ShaderMask(
        shaderCallback: (bounds) =>
            LinearGradient(colors: [Colors.blue, Colors.teal]).createShader(
          bounds,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: SizedBox(
            width: isPortrait
                ? screenWidth * portraitSize
                : screenWidth * landscapeSize,
            height: screenHeight * 0.1,
            child: ElevatedButton(
              onPressed: gameData.isShow == false
                  ? () => setState(() {
                        if (gameData.choice[3] == gameData.ans) {
                          gameData.increScore();
                        }
                        gameData.checkAns(gameData.choice[3]);
                        gameData.randomEquation();
                      })
                  : null,
              child: Text(
                '${gameData.choice[3]}',
                style: TextStyle(fontSize: 30),
              ),
            ),
          ),
        ),
      );
    }

    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          ? Container(
              // decoration: const BoxDecoration(
              //   gradient: LinearGradient(
              //     begin: Alignment.topLeft,
              //     end: Alignment.bottomRight,
              //     colors: [Color(0xff5936B4), Color.fromARGB(255, 41, 32, 102)],
              //   ),
              // ),
              constraints: const BoxConstraints.expand(),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("./img/background.png"),
                    fit: BoxFit.cover),
              ),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //เวลา
                      Text(
                        "เวลา : $_start",
                        style: TextStyle(color: Colors.amber, fontSize: 32),
                      ),

                      Stack(
                        children: [
                          //โจทย์
                          Visibility(
                            visible: !gameData.isShow,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: Container(
                                color: Colors.white,
                                width: screenWidth * 0.8,
                                height: screenHeight * 0.25,
                                child: Center(
                                  child: Text(
                                    "${gameData.varA} ${gameData.operator} ${gameData.varB} = ?",
                                    style: TextStyle(fontSize: 30),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //ถูกผิด
                          Visibility(
                            visible: gameData.isShow,
                            child: ClipRRect(
                              child: Container(
                                width: screenWidth * 0.6,
                                height: screenHeight * 0.25,
                                child: Center(
                                  child: Image(
                                      image: AssetImage('${gameData.pic}'),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                      //ข้อความ
                      ShaderMask(
                        shaderCallback: (bounds) => const LinearGradient(
                                colors: [Colors.red, Colors.orangeAccent])
                            .createShader(
                          bounds,
                        ),
                        child: const Text(
                          'จงเลือกคำตอบที่ถูกต้อง',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 32,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      //ปุ่ม
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            choiceBtn1(),
                            choiceBtn2(),
                            // choiceBtn3(),
                            // choiceBtn4(),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            choiceBtn3(),
                            choiceBtn4(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          : Container(
              // decoration: const BoxDecoration(
              //   gradient: LinearGradient(
              //     begin: Alignment.topLeft,
              //     end: Alignment.bottomRight,
              //     colors: [Color(0xff5936B4), Color.fromARGB(255, 41, 32, 102)],
              //   ),
              // ),
              constraints: const BoxConstraints.expand(),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("./img/background.png"),
                    fit: BoxFit.cover),
              ),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //เวลา
                      Text(
                        "เวลา : $_start",
                        style: TextStyle(color: Colors.amber, fontSize: 32),
                      ),

                      Stack(
                        children: [
                          //โจทย์
                          Visibility(
                            visible: !gameData.isShow,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: Container(
                                color: Colors.white,
                                width: screenWidth * 0.6,
                                height: screenHeight * 0.45,
                                child: Center(
                                  child: Text(
                                    "${gameData.varA} ${gameData.operator} ${gameData.varB} = ?",
                                    style: TextStyle(fontSize: 60),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          //ถูกผิด
                          Visibility(
                            visible: gameData.isShow,
                            child: ClipRRect(
                              child: Container(
                                width: screenWidth * 0.6,
                                height: screenHeight * 0.45,
                                child: Center(
                                  child: Image(
                                      image: AssetImage('${gameData.pic}'),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                      //ข้อความ
                      ShaderMask(
                        shaderCallback: (bounds) => const LinearGradient(
                                colors: [Colors.red, Colors.orangeAccent])
                            .createShader(
                          bounds,
                        ),
                        child: const Text(
                          'จงเลือกคำตอบที่ถูกต้อง',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 32,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      //ปุ่ม
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            choiceBtn1(),
                            choiceBtn2(),
                            choiceBtn3(),
                            choiceBtn4(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
    });
  }
}
