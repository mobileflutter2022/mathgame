import 'dart:html';
import 'dart:js';

import 'package:flutter/material.dart';
import 'package:math_game/game.dart';
import 'package:provider/provider.dart';

import './game_provider.dart';

class Level extends StatelessWidget {
  const Level({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final gameData = Provider.of<GameData>(context, listen: false);
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          ? Container(
              constraints: const BoxConstraints.expand(),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("imgLevel/bglv.png"),
                    fit: BoxFit.cover),
              ),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          'เลือกระดับความยาก',
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 255, 255, 255)),
                        ),
                      ),
                      Container(
                        height: 70,
                      ),
                      ShaderMask(
                        shaderCallback: (bounds) => LinearGradient(colors: [
                          Colors.blue,
                          Color.fromARGB(255, 150, 0, 0)
                        ]).createShader(
                          bounds,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              width: 120,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 20,
                                ),
                                onPressed: () {
                                  gameData.level = 1;

                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Game()));
                                },
                                child: const Text(
                                  '1',
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 255, 255, 255),
                                      fontSize: 80),
                                ),
                              ),
                            ),
                            Container(
                              width: 120,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 20,
                                ),
                                onPressed: () {
                                  gameData.level = 2;
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Game()));
                                },
                                child: const Text(
                                  '2',
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 255, 255, 255),
                                      fontSize: 80),
                                ),
                              ),
                            ),
                            Container(
                              width: 120,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 20,
                                ),
                                onPressed: () {
                                  gameData.level = 3;
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Game()));
                                },
                                child: const Text(
                                  '3',
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 255, 255, 255),
                                      fontSize: 80),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          : Container(
              constraints: const BoxConstraints.expand(),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("imgLevel/bglv.png"),
                    fit: BoxFit.cover),
              ),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: Container(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            'เลือกระดับความยาก',
                            style: TextStyle(
                                fontSize: 40,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 255, 255, 255)),
                          ),
                        ),
                        Container(
                          height: 70,
                        ),
                        ShaderMask(
                          shaderCallback: (bounds) => LinearGradient(colors: [
                            Colors.blue,
                            Color.fromARGB(255, 150, 0, 0)
                          ]).createShader(
                            bounds,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                width: 120,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    elevation: 20,
                                  ),
                                  onPressed: () {
                                    gameData.level = 1;

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => Game()));
                                  },
                                  child: const Text(
                                    '1',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        fontSize: 80),
                                  ),
                                ),
                              ),
                              Container(
                                width: 120,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    elevation: 20,
                                  ),
                                  onPressed: () {
                                    gameData.level = 2;

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => Game()));
                                  },
                                  child: const Text(
                                    '2',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        fontSize: 80),
                                  ),
                                ),
                              ),
                              Container(
                                width: 120,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    elevation: 20,
                                  ),
                                  onPressed: () {
                                    gameData.level = 3;

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => Game()));
                                  },
                                  child: const Text(
                                    '3',
                                    style: TextStyle(
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        fontSize: 80),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ]),
                ),
              ),
            );
    });
  }
}



// var body = ListView(
//   children: <Widget>[
//     Column(
//       children: <Widget>[
//         Container(
//           height: 200,
//         ),
//         DefaultTextStyle.merge(
//           style: TextStyle(color: Color.fromARGB(255, 255, 255, 255)),
//           child: Center(
//             child: Text(
//               "LEVEL.",
//               textAlign: TextAlign.center,
//               style: new TextStyle(fontSize: 60),
//             ),
//           ),
//         ),
//         chooseLV(),
//       ],
//     ),
//   ],
// );

// Widget chooseLV() {
//   return Padding(
//     padding: const EdgeInsets.all(4.0),
//     child: Row(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: <Widget>[
//         Container(
//           height: 200,
//         ),
//         Container(
//           width: 120,
//           child: ElevatedButton(
//             style: ElevatedButton.styleFrom(
//               elevation: 20,
//             ),
//             onPressed: () {},
//             child: const Text(
//               '1',
//               style:
//                   TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 80),
//             ),
//           ),
//         ),
//         Container(
//           width: 120,
//           child: ElevatedButton(
//             style: ElevatedButton.styleFrom(
//               elevation: 20,
//             ),
//             onPressed: () {},
//             child: const Text(
//               '2',
//               style:
//                   TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 80),
//             ),
//           ),
//         ),
//         Container(
//           width: 120,
//           child: ElevatedButton(
//             style: ElevatedButton.styleFrom(
//               elevation: 20,
//             ),
//             onPressed: () {
             
//             },
//             child: const Text(
//               '3',
//               style:
//                   TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 80),
//             ),
//           ),
//         ),
//       ],
//     ),
//   );
// }
