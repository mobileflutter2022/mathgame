import 'package:flutter/material.dart';
import 'game.dart';
import 'level.dart';
import 'home.dart';
import 'package:provider/provider.dart';

import './game_provider.dart';

class Score extends StatelessWidget {
  const Score({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final gameData = Provider.of<GameData>(context, listen: false);
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return OrientationBuilder(builder: (context, orientation) {
      return isPortrait
          // แนวตั้ง
          ? Container(
              constraints: const BoxConstraints.expand(),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("./img/background.png"),
                    fit: BoxFit.cover),
              ),
              child: Scaffold(
                  backgroundColor: Colors.transparent,
                  body: Container(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                        Padding(padding: EdgeInsets.all(20)),
                        Image.asset("./images/score.png"),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            '${gameData.score} คะแนน',
                            style: TextStyle(
                                fontSize: 40, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Image.asset("./images/pilot.png"),
                        Container(
                          // alignment: FractionalOffset.bottomCenter,
                          child: TextButton.icon(
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.black,
                              padding: const EdgeInsets.all(10.0),
                              textStyle: const TextStyle(fontSize: 30),
                            ),
                            icon: const Icon(Icons.refresh_rounded),
                            label: const Text('เริ่มเกมใหม่'),
                            onPressed: () {
                              gameData.score = 0;
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) => new Level()));
                            },
                          ),
                        )
                      ]))))
          // แนวนอน
          : Container(
              constraints: const BoxConstraints.expand(),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("./img/background.png"),
                    fit: BoxFit.cover),
              ),
              child: Scaffold(
                  backgroundColor: Colors.transparent,
                  body: Container(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                        // Padding(padding: EdgeInsets.all(20)),
                        Image.asset("./images/score.png"),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            '${gameData.score} คะแนน',
                            style: TextStyle(
                                fontSize: 40, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "./images/pilot.png",
                              width: 200,
                              height: 200,
                            ),
                            Container(
                              // alignment: FractionalOffset.bottomCenter,
                              child: TextButton.icon(
                                style: TextButton.styleFrom(
                                  foregroundColor: Colors.black,
                                  padding: const EdgeInsets.all(10.0),
                                  textStyle: const TextStyle(fontSize: 30),
                                ),
                                icon: const Icon(Icons.refresh_rounded),
                                label: const Text('เริ่มเกมใหม่'),
                                onPressed: () {
                                  gameData.score = 0;
                                  Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                          builder: (context) => new Level()));
                                },
                              ),
                            )
                          ],
                        ),
                      ]))));
    });

    // Container(
    //     constraints: const BoxConstraints.expand(),
    //     decoration: const BoxDecoration(
    //       image: DecorationImage(
    //           image: NetworkImage("./img/background.png"), fit: BoxFit.cover),
    //     ),
    //     child: Scaffold(
    //         backgroundColor: Colors.transparent,
    //         body: Container(
    //             child: Column(children: <Widget>[
    //           Padding(padding: EdgeInsets.all(20)),
    //           Image.asset("./images/score.png"),
    //           Align(
    //             alignment: Alignment.center,
    //             child: Text(
    //               '100 คะแนน',
    //               style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
    //             ),
    //           ),
    //           Image.asset("./images/pilot.png"),
    //           Container(
    //             // alignment: FractionalOffset.bottomCenter,
    //             child: TextButton.icon(
    //               style: TextButton.styleFrom(
    //                 foregroundColor: Colors.black,
    //                 padding: const EdgeInsets.all(10.0),
    //                 textStyle: const TextStyle(fontSize: 30),
    //               ),
    //               icon: const Icon(Icons.refresh_rounded),
    //               label: const Text('เริ่มเกมใหม่'),
    //               onPressed: () {},
    //             ),
    //           )
    //         ]))));
  }
}
